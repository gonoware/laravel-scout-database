# Laravel Scout Database Driver

[![GitLab Repository](https://img.shields.io/badge/GitLab-gonoware/laravel--scout--database-blue.svg?logo=gitlab&style=flat-square&longCache=true)](https://gitlab.com/gonoware/laravel-scout-database)
[![Laravel Version](https://img.shields.io/badge/Laravel-5.5-blue.svg?logo=laravel&style=flat-square&longCache=true)]()
[![Latest Stable Version](https://poser.pugx.org/gonoware/laravel-scout-database/v/stable?format=flat-square)](https://packagist.org/packages/gonoware/laravel-scout-database)
[![StyleCI](https://gitlab.styleci.io/repos/8769147/shield)](https://gitlab.styleci.io/repos/8769147)
[![License](https://poser.pugx.org/gonoware/laravel-scout-database/license?format=flat-square)](https://packagist.org/packages/gonoware/laravel-scout-database)
[![Total Downloads](https://poser.pugx.org/gonoware/laravel-scout-database/downloads?format=flat-square)](https://packagist.org/packages/gonoware/laravel-scout-database)

> Forked from [github.com/boxed-code/laravel-scout-database](https://github.com/boxed-code/laravel-scout-database)

This is a basic database backed driver [for Laravel Scout](https://laravel.com/docs/master/scout). It is intended for use during development to avoid the need to setup an elastic instance or agolia and instead uses your active database configuration. 

Searchable model attributes are JSON encoded an placed in a text column for simplicity, the very primitive 'like' operator is used to perform queries. It is fully functional supporting additional where clauses, etc. The driver deliberately avoids using free text queries & indexes as these are somewhat provider specific and would prevent the goal of it being able to operate with any architecture.

This driver is zero configuration, requiring you to only add the service provider & run the migration.


## Installation

This package can be installed through Composer.

```bash
composer require gonoware/laravel-scout-database
```

Laravel 5.5+ uses Package Auto-Discovery, so doesn't require you to manually add
the ServiceProvider.

Then run the migrations via the console
```shell
php artisan migrate
```


## Usage

When the installation is done set `SCOUT_DRIVER='database'` in your `.env`.

Now you can use Laravel Scout as described in the [official documentation](https://laravel.com/docs/master/scout)

https://laravel.com/docs/master/scout#indexing


## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.


## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.


## Security

If you discover any security related issues, please email [em@gonoware.com](mailto:em@gonoware.com) 
instead of using the issue tracker.


## Credits

- [Emanuel Mutschlechner](https://gitlab.com/emanuel)
- [Benedikt Tuschter](https://gitlab.com/benedikttuschter)
- [All Contributors](https://gitlab.com/gonoware/laravel-analytics/graphs/master)
- [GitHub Contributors](https://github.com/boxed-code/laravel-scout-database/graphs/contributors)


## License

[MIT](LICENSE.md)
 
Copyright (c) 2018-present Go NoWare
 
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgitlab.com%2Fgonoware%2Flaravel-scout-database.svg?type=large)](https://app.fossa.io/projects/git%2Bgitlab.com%2Fgonoware%2Flaravel-scout-database?ref=badge_large)
