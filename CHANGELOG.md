# Changelog

All notable changes will be documented in this file.

## 1.0.0 - 2018-10-09

- Bump scout version to 5.0
