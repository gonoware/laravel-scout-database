<?php

namespace GoNoWare\Laravel\Scout;

use Illuminate\Support\ServiceProvider;
use Laravel\Scout\EngineManager;

class DatabaseEngineServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../migrations');

        resolve(EngineManager::class)->extend('database', function () {
            return new DatabaseEngine($this->app['db']);
        });
    }
}
